# Plant and Food Research - CKAN Metadata Catalogue
# Services for Excel Spreadsheets.

This project contains VBA scripts for Excel that allow the lookup of project codes from the CKAN API and populating metadata pages in an Excel file. 

It includes VBA Macro files that provide a WebClient to support the calling of the CKAN API and filling in metadata page details

It provides an Excel Plug-in (xlam) that adds these functions to the ribbon bar.



