Attribute VB_Name = "PFRMergeFromWorkbook"

Public Sub MergeFromWorkbook()
    'Find and open the target workbook - then copy over all the sheets
    Dim targetBook As Workbook
    Dim sourceBook As Workbook
    Dim targetName As String
    
    Set targetBook = ActiveWorkbook
    Path = FileBrowse()
    If Path = "" Then
        Exit Sub
    End If
    ' Open method requires full file path to be referenced, but workbook calls only need the name
    targetName = Right(Path, Len(Path) - InStrRev(Path, Application.PathSeparator, , 1))
    Set sourceBook = Workbooks.Open(Path, ReadOnly:=True)
    sourceBook.Sheets.Copy After:=targetBook.Sheets(targetBook.Sheets.Count)
    sourceBook.Close SaveChanges:=False
    Sheet1.Activate
    'UpdateSheetIndex  - don't call this as we don't know for sure we are on ProjectDescription page
End Sub
