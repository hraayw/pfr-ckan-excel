Attribute VB_Name = "PFRReferenceAPIPeople"
Function GetPeopleRefAPI(ByVal search As String, ByVal test As Boolean)
'
' GetProjectCode Macro
' Call API to check find matching people
'
    Dim ApiClient As New WebClient
    Dim Auth As New PFRApiAuthenticator
    Set ApiClient.Authenticator = Auth
    WebHelpers.EnableLogging = True

    ApiClient.BaseUrl = ReferenceAPIHost(test)

    ' Use GetJSON helper to execute simple request and work with response
    Dim Response As WebResponse
    Set GetPeopleRefAPI = ApiClient.GetJson("/api/people/?family_name__icontains=" & search)
    
End Function
