VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} FindPersonForm 
   Caption         =   "Find PFR Person"
   ClientHeight    =   3960
   ClientLeft      =   40
   ClientTop       =   400
   ClientWidth     =   4220
   OleObjectBlob   =   "FindPersonForm.frx":0000
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "FindPersonForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CancelButton_Click()
    Unload Me
End Sub

Private Sub Label1_Click()
    Label1.Visible = False
End Sub

Private Sub NamesList_Click()
' insert name into the current cell
    ActiveCell.Value = NamesList.Value
End Sub

Private Sub NameText_Change()
    
    If Len(NameText.Text) <> 0 Then
        Label1.Visible = False
    Else
        Label1.Visible = True
        ResultsLabel = "Click below to add name to current cell"
    End If
    
    NamesList.Clear
    If Len(NameText.Text) > 2 Then
        SubmitButton_Click
    End If
End Sub

Private Sub SubmitButton_Click()
    ' Unload Me
    ResultsLabel = "finding:" & NameText
    Set Response = GetPeopleRefAPI(NameText, False)
    If Response.StatusCode <> WebStatusCode.Ok Then
        ResultsLabel = "Error:" & Response.StatusCode
        Exit Sub
    End If
    CountResults = Response.Data("count")
    If CountResults = 0 Then
        ResultsLabel = "No matches"
        Exit Sub
    Else
        ResultsLabel = CountResults & " found, select name to insert in current cell"
        Set Results = Response.Data("results")
        ' NamesList.Clear
        For Each Person In Results
            FoundName = Person("given_name") & " " & Person("family_name")
            NamesList.AddItem FoundName
        Next Person
    End If
    ' Response
End Sub


Private Sub NameText_Enter()
'    NamesList.SetFocus
End Sub

Private Sub UserForm_Initialize()
    Label1.Visible = True
    ResultsLabel = "Enter PFR staff surname"
    NameText.SetFocus
    
End Sub
Attribute VB_Name = "FindPersonForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub CancelButton_Click()
    Unload Me
End Sub

Private Sub Label1_Click()
    Label1.Visible = False
End Sub

Private Sub NamesList_Click()
' insert name into the current cell
    ActiveCell.Value = NamesList.Value
End Sub

Private Sub NameText_Change()
    
    If Len(NameText.Text) <> 0 Then
        Label1.Visible = False
    Else
        Label1.Visible = True
        ResultsLabel = "Click below to add name to current cell"
    End If
    
    NamesList.Clear
    If Len(NameText.Text) > 2 Then
        SubmitButton_Click
    End If
End Sub

Private Sub SubmitButton_Click()
    ' Unload Me
    ResultsLabel = "finding:" & NameText
    Set Response = GetPeopleRefAPI(NameText, True)
    If Response.StatusCode <> WebStatusCode.Ok Then
        ResultsLabel = "Error:" & Response.StatusCode
        Exit Sub
    End If
    CountResults = Response.Data("count")
    If CountResults = 0 Then
        ResultsLabel = "No matches"
        Exit Sub
    Else
        ResultsLabel = CountResults & " found, select name to insert in current cell"
        Set Results = Response.Data("results")
        ' NamesList.Clear
        For Each Person In Results
            FoundName = Person("given_name") & " " & Person("family_name")
            NamesList.AddItem FoundName
        Next Person
    End If
    ' Response
End Sub


Private Sub NameText_Enter()
'    NamesList.SetFocus
End Sub

Private Sub UserForm_Initialize()
    Label1.Visible = True
    ResultsLabel = "Enter PFR staff surname"
    NameText.SetFocus
    
End Sub
