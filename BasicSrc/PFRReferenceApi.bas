Attribute VB_Name = "PFRReferenceApi"
Public Sub CheckReferenceAPI(test, t2)
    refApiUrl = ReferenceAPIHost(test)
    MsgBox "Problem connecting to PFR Reference API at " + refApiUrl
End Sub


Public Function ReferenceAPIHost(test)
    If (test) Then
        ReferenceAPIHost = "http://localhost:8800"
    Else
        ReferenceAPIHost = "http://iris.uat.pfr.co.nz:8800"
    End If
End Function
Public Function ReferenceAPIToken(test)
    If (test) Then
        ReferenceAPIToken = "9485098571097510"
    Else
        ReferenceAPIToken = "1fff87f414becac700d9ec16716d7691377776f4" ' for hraayw
    End If
End Function

Sub GetProjectCodeRefAPI(ByVal search As String, ByVal test As Boolean)
'
' GetProjectCode Macro
' Call API to check a project code in search
'
    Dim ApiClient As New WebClient
    Dim Auth As New PFRApiAuthenticator
    Set ApiClient.Authenticator = Auth
    WebHelpers.EnableLogging = True

    ApiClient.BaseUrl = ReferenceAPIHost(test)

    ' Use GetJSON helper to execute simple request and work with response
    Dim Response As WebResponse
    Set Response = ApiClient.GetJson("/api/projects/?search=" & search)
    
    If Response.StatusCode <> WebStatusCode.Ok Then
        [ErrorMessage].Value = "Error:" & Response.StatusCode
        Exit Sub
    End If
    If Response.Data("count") = 0 Then
        [Search_results].Value = "No matches"
        Exit Sub
    End If
    ProcessProjectRefs Response
    
End Sub


Sub ProcessProjectRefs(Response As WebResponse)
    ' list search results
    If Response.Data("count") <> 1 Then 'Fill in the search results
        Set Results = Response.Data("results")
        [Search_results].Value = Response.Data("count") & " matching results, using first"
    
        row = 4
        For Each Prg In Results
             Cells(row, 6).Value = Prg("local_project_identifier")
             Cells(row, 7).Value = Prg("project_title")
             ' Cells(row, 8).Value = Prg("project_start_date")
             row = row + 1
        Next Prg
    End If
    
    ' fill in first result
    Set Results = Response.Data("results")
    Dim Project As Dictionary
    Set Project = Results(1)

    [Project_code].Value = Project("local_project_identifier")
    [project_title].Value = Project("project_title")
   ' [Project_leader].Value = Project("project_leader_description")
   ' [Lead_researcher].Value = Project("PC Approver")
   ' [Funding_source].Value = Project("funding_source_description")
  
    [project_start_date].Value = ISODATEZ(Project("project_start_date"))
    [project_end_date].Value = ISODATEZ(Project("project_end_date"))

'    If (Project("industry_description") <> "Not assigned") Then
'        [Keywords].Value = Project("industry_description")
'    End If
    projectid = Project("id")
    GetProjectStaffRefAPI projectid, False
    
    
End Sub

Sub GetProjectStaffRefAPI(ByVal id As String, ByVal test As Boolean)
'Sub GetProjectCodeRefAPI(ByVal search As String)
'
' GetProjectCode Macro
' Call API to check a project code in search
'
    Dim ApiClient As New WebClient
    Dim Auth As New PFRApiAuthenticator
    Set ApiClient.Authenticator = Auth
    WebHelpers.EnableLogging = True

    ApiClient.BaseUrl = ReferenceAPIHost(test)

    ' Use GetJSON helper to execute simple request and work with response
    Dim Response As WebResponse
    Set Response = ApiClient.GetJson("/api/projects/" & id & "/assignedstaff/")
    
    If Response.StatusCode <> WebStatusCode.Ok Then
        [ErrorMessage].Value = "Error:" & Response.StatusCode
        Exit Sub
    End If
    If Response.Data("count") = 0 Then
        [ErrorMessage].Value = "No people found"
        Exit Sub
    End If
    ProcessProjectStaff Response
    
End Sub


Sub ProcessProjectStaff(Response As WebResponse)
    Set Results = Response.Data("results")
    row = 4
    For Each Item In Results
        Set Role = Item("role")
        Set Person = Item("person")
        name = Person("given_name") & " " & Person("family_name")
        role_code = Role("code")
        role_description = Role("description")
        Cells(row, 9).Value = name
        Cells(row, 10).Value = role_code
        Cells(row, 11).Value = role_description
        row = row + 1
        Debug.Print name, role_code, role_description
        If (role_code = "R1") Then
            [Project_leader].Value = name
        End If
    Next Item
   ' [Project_leader].Value = Project("project_leader_description")
   ' [Lead_researcher].Value = Project("PC Approver")
   ' [Funding_source].Value = Project("funding_source_description")
    
End Sub


Public Function ISODATEZ(iso As String) As Date
    Dim yearPart As Integer: yearPart = CInt(Mid(iso, 1, 4))
    Dim monPart As Integer: monPart = CInt(Mid(iso, 6, 2))
    Dim dayPart As Integer: dayPart = CInt(Mid(iso, 9, 2))
    Dim hourPart As Integer: hourPart = CInt(Mid(iso, 12, 2))
    Dim minPart As Integer: minPart = CInt(Mid(iso, 15, 2))
    Dim secPart As Integer: secPart = CInt(Mid(iso, 18, 2))
    Dim tz As String: tz = Mid(iso, 20)

    Dim dt As Date: dt = DateSerial(yearPart, monPart, dayPart) + TimeSerial(hourPart, minPart, secPart)

    ' Add the timezone
    If tz <> "" And Left(tz, 1) <> "Z" Then
        Dim colonPos As Integer: colonPos = InStr(tz, ":")
        If colonPos = 0 Then colonPos = Len(tz) + 1

        Dim minutes As Integer: minutes = CInt(Mid(tz, 2, colonPos - 2)) * 60 + CInt(Mid(tz, colonPos + 1))
        If Left(tz, 1) = "+" Then minutes = -minutes
        dt = DateAdd("n", minutes, dt)
    End If

    ' Return value is the ISO8601 date in the local time zone
    ' dt = UTCToLocalTime(dt)
    ISODATEZ = dt
End Function
