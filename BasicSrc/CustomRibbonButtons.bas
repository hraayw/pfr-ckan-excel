Attribute VB_Name = "CustomRibbonButtons"
Option Explicit
Private Const msMODULE As String = "CustomRibbonButtons"


Sub Custom_Btn_ClearMetadata(control As IRibbonControl)
  Call ClearMetadata
End Sub
Sub Custom_Btn_ClearSearch(control As IRibbonControl)
  Call ClearSearch
End Sub
Sub Custom_Btn_GetProjectCode(control As IRibbonControl)
  Call GetProjectCode
End Sub
Sub Custom_Btn_UpdateSheetIndex(control As IRibbonControl)
  Call UpdateSheetIndex
End Sub
Sub Custom_Btn_UpdateDataHub(control As IRibbonControl)
  Call UpdateDataHub
End Sub
Sub Custom_Btn_MergeFromWorkbook(control As IRibbonControl)
  Call MergeFromWorkbook
End Sub

' Sub Custom_Btn_FindPerson(control As IRibbonControl)
'   Call FindPerson(findPersonName)
' End Sub
' Public Sub EditBoxTextChanged(ByRef control As Office.IRibbonControl, ByRef Text As String)
'     findPersonName = Text
'     Debug.Print Text
    
' End Sub

