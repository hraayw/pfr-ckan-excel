Attribute VB_Name = "PFRDataHubCollections"

Public Sub GetCollections()
    name = [Username].Value
    If Environ("Username") <> "" Then
        name = Environ("Username")
        [Username].Value = name
    End If
    
    Dim CkanClient As New WebClient
    CkanClient.BaseUrl = DataHubHost() & "/api/3/action/"
    [ErrorMessage].Value = ""
    
    Dim Request As New WebRequest
    Request.Method = WebMethod.HttpGet
    Request.Resource = "organization_list_for_user?permission=create_dataset&id=" & LCase(name)
    Request.Format = WebFormat.Json
    Request.AddHeader "Authorization", CkanApiKey()
    
    ' Enable logging before the request is executed
    WebHelpers.EnableLogging = True
    
    ' Use GetJSON helper to execute simple request and work with response
    Dim Response As WebResponse
    Set Response = CkanClient.Execute(Request)
    Debug.Print Response.StatusCode
    If Response.StatusCode <> WebStatusCode.Ok Then
        ReportErr Response.StatusCode, Response.StatusDescription
        Exit Sub
    End If

    Dim Result As Collection
    Set Result = Response.Data("result")
    
    If Result.Count = 0 Then
        Exit Sub
    End If
    ReDim CollectionsList(Result.Count)
    With ActiveSheet.DropDowns("CollectionComboBox")
        .RemoveAllItems
        i = 0
        For Each Item In Result
           .AddItem Item("display_name")
           CollectionsList(i) = Item("name")
           i = i + 1
           
        Next Item
        .Value = 1
    End With
    ' save the collections list in sheet
    ' one row spanning several columns
    [CollectionsClear].Value = ""
    [CollectionsCount].Value = UBound(CollectionsList)
    Dim Destination As Range
    Set Destination = Range("CollectionsList")
    Set Destination = Destination.Resize(UBound(CollectionsList), 1)
    Destination.Value = Application.Transpose(CollectionsList)
End Sub


Public Sub GetGroups()
    name = [Username].Value
    Dim CkanClient As New WebClient
    CkanClient.BaseUrl = DataHubHost() & "/api/3/action/"
    [ErrorMessage].Value = ""
    
    Dim Request As New WebRequest
    Request.Method = WebMethod.HttpGet
    Request.Resource = "group_list?all_fields=true&include_users=true"
    Request.Format = WebFormat.Json
    Request.AddHeader "Authorization", CkanApiKey()
    
    ' Enable logging before the request is executed
    WebHelpers.EnableLogging = True
    
    ' Use GetJSON helper to execute simple request and work with response
    Dim Response As WebResponse
    Set Response = CkanClient.Execute(Request)
    If Response.StatusCode <> WebStatusCode.Ok Then
        ReportErr Response.StatusCode, Response.StatusDescription
        Exit Sub
    End If


    Dim Result As Collection
    Set Result = Response.Data("result")
    
    If Result.Count = 0 Then
        Exit Sub
    End If
    ReDim GroupsList(Result.Count)

    With ActiveSheet.ListBoxes("GroupsListBox")
        .RemoveAllItems
        groupCtr = 0
        For Each Item In Result
            If name <> "" Then
                ' only list groups the person can update
                For Each User In Item("users")
                    If User("name") = name Then
                        .AddItem Item("display_name")
                        GroupsList(groupCtr) = Item("name")
                        groupCtr = groupCtr + 1
                    End If
                Next User
            Else
                ' list all groups
                .AddItem Item("display_name")
                GroupsList(groupCtr) = Item("name")
                groupCtr = groupCtr + 1
            End If
        Next Item
        If groupCtr <> 0 Then
            .Value = 1
        End If
    End With
    ' save the groups list in sheet
    ' one row spanning several columns
    [GroupsClear].Value = ""
    [GroupsCount].Value = groupCtr
    
    Dim Destination As Range
    Set Destination = Range("GroupsList")
    Set Destination = Destination.Resize(UBound(GroupsList), 1)
    Destination.Value = Application.Transpose(GroupsList)
End Sub

Public Sub GetLists()
    ' Get Collections list from table
    Dim Source As Range
    Set Source = Range("CollectionsList")
    If Source.Cells.Count = 1 Then
        List = Source.Value
        ReDim CollectionsList(1)
        CollectionsList(1) = List
    Else
        Set Source = Source.Resize([CollectionsCount].Value, 1)
        List = Source.Value
        ReDim CollectionsList(UBound(List))
        For row = 1 To UBound(List)
            CollectionsList(row - 1) = List(row, 1)
        Next row
    End If
    ' Get Groups list from table
    Set Source = Range("GroupsList")
    Set Source = Source.Resize([GroupsCount].Value, 1)
    List = Source.Value

    ReDim GroupsList(UBound(List))
    For row = 1 To UBound(List)
    x = List(row, 1)
        GroupsList(row) = List(row, 1)
    Next row
    Z = GroupsList
End Sub

Sub ClearCombos()
    ActiveSheet.DropDowns("CollectionComboBox").RemoveAllItems
    ActiveSheet.ListBoxes("GroupsListBox").RemoveAllItems
End Sub

