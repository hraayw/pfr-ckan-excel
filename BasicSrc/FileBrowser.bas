Attribute VB_Name = "FileBrowser"
Function FileBrowse()
    ' Test for the operating system.
        If Not Application.OperatingSystem Like "*Mac*" Then
            ' Is Windows.
            FileBrowse = FileBrowseWindows
        Else
            ' Is a Mac and will test if running Excel 2011 or higher.
            If Val(Application.Version) > 14 Then
                FileBrowse = FileBrowseMac
            End If
        End If
    End Function
Function FileBrowseWindows()
 ' Open GetOpenFilename with the file filters.
 
    FName = Application.GetOpenFilename( _
        FileFilter:="Microsoft Excel Workbooks (*.xls;*.xlsx;*.xlsm),*.xls;*.xlsx;*.xlsm", _
        Title:="Select a Workbook to copy the Cover Sheet to", _
        MultiSelect:=False)
    FileBrowseWindows = FName
End Function

Function FileBrowseMac()
    Dim MyPath As String
    Dim MyScript As String
    Dim MyFiles As String
    Dim MySplit As Variant
    Dim N As Long
    Dim FName As String
    Dim mybook As Workbook

    On Error Resume Next
    MyPath = MacScript("return (path to documents folder) as String")
    'Or use MyPath = "Macintosh HD:Users:Ron:Desktop:TestFolder:"

    ' In the following statement, change true to false in the line "multiple
    ' selections allowed true" if you do not want to be able to select more
    ' than one file. Additionally, if you want to filter for multiple files, change
    ' {""com.microsoft.Excel.xls""} to
    ' {""com.microsoft.excel.xls"",""public.comma-separated-values-text""}
    ' if you want to filter on xls and csv files, for example.
    MyScript = _
    "set applescript's text item delimiters to "","" " & vbNewLine & _
    "set theFiles to (choose file of type " & _
    " {""org.openxmlformats.spreadsheetml.sheet"",""com.microsoft.Excel.xls"",""org.openxmlformats.spreadsheetml.sheet.macroenabled""} " & _
    "with prompt ""Please select target Workbook"" default location alias """ & _
    MyPath & """ multiple selections allowed false) as string" & vbNewLine & _
    "set applescript's text item delimiters to """" " & vbNewLine & _
    "return theFiles"

    wbName = MacScript(MyScript)
    If CInt(Split(Application.Version, ".")(0)) >= 15 Then 'excel 2016 support
        wbName = Replace(wbName, ":", "/")
        wbName = Replace(wbName, "Macintosh HD", "", Count:=1)
    End If
    FileBrowseMac = wbName
    On Error GoTo 0

End Function
    
Function bIsBookOpen(ByRef szBookName As String) As Boolean
' Contributed by Rob Bovey
    On Error Resume Next
    bIsBookOpen = Not (Application.Workbooks(szBookName) Is Nothing)
End Function


