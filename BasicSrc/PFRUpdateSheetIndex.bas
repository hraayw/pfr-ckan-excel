Attribute VB_Name = "PFRUpdateSheetIndex"
Public Sub UpdateSheetIndex()
    Dim ws As Worksheet
    Dim Counter As Integer
    
    Counter = 1
    Range("SheetIndex").Clear
    For Each ws In ActiveWorkbook.Worksheets
        link = "#'" & ws.name & "'!A1"
        ActiveSheet.Hyperlinks.Add Range("SheetIndex").Cells(Counter, 1), link, "", "", ws.name
        Counter = Counter + 1
    Next ws
End Sub

