Attribute VB_Name = "PFRDataHub"
' Plant and Food Research - DataHub Macros
' This module provides supporting macros for the Metadata Coversheet template
' This template provides a cover sheet for any Excel workbook that is used to hold research data
' The cover sheet captures key metadata about the project and allows the information to be registered into the PFR DataHub.

Public Sub UpdateSheetIndex()

    Dim ws As Worksheet
    Dim Counter As Integer

    Counter = 1
    Range("SheetIndex").Clear
    For Each ws In ActiveWorkbook.Worksheets
        link = "#'" & ws.Name & "'!A1"
        ActiveSheet.Hyperlinks.Add Range("SheetIndex").Cells(Counter, 1), link, "", "", ws.Name
        Counter = Counter + 1
    Next ws
End Sub
Function BaseUrl()
    TestMode = False
    If (TestMode) Then
        BaseUrl = "http://ckan:5000"
    Else
        BaseUrl = "http://datahub.plantandfood.co.nz"
    End If
End Function
Function CkanApiKey()
    TestMode = False
    If (TestMode) Then
        CkanApiKey = "c363a232-44eb-4596-90aa-dc2c23e67f58"
    Else
        CkanApiKey = "12809ca3-bad1-4a51-93b4-32cc38c76ee2"
    End If
End Function


Sub GetProjectCode()
Attribute GetProjectCode.VB_Description = "Call API to check a project code in cell B2"
Attribute GetProjectCode.VB_ProcData.VB_Invoke_Func = " \n14"
'
' GetProjectCode Macro
' Call API to check a project code in cell B2
'
    Dim CkanClient As New WebClient
    ' Enable logging before the request is executed
    WebHelpers.EnableLogging = True

    CkanClient.BaseUrl = BaseUrl() & "/api/3/action/"
    [ErrorMessage].Value = ""
    Dim ProjectCode As String
    ProjectCode = Range("B2").Value
    
    Dim resource_id As String
    resource_id = "pfr-projectcodes"
   
    Dim Query As String
    Query = "datastore_search?q=" & ProjectCode & "&resource_id=" & resource_id
 
    ' Use GetJSON helper to execute simple request and work with response
    Dim Response As WebResponse
    Set Response = CkanClient.GetJson(Query)
    ProcessProjectInfo Response

End Sub
Public Sub ClearMetadata()
    ' wipe out the filled in fields so that the sheet is clean as the original template
    [MainMetadata].Value = ""
    [SearchResults].Value = ""
    [AssociatedRawData].Value = ""
    [otherResources].Value = ""
End Sub
Public Sub ClearSearch()
    ' wipe out the search results
    [SearchResults].Value = ""
End Sub

Public Sub ProcessProjectInfo(Response As WebResponse)
    
    If Response.StatusCode <> WebStatusCode.Ok Then
        [ErrorMessage].Value = "Error:" & Response.StatusCode
        Exit Sub
    End If

    Dim Result As Dictionary
    Set Result = Response.Data("result")
    If Result("records").Count <> 1 Then
        [ErrorMessage].Value = Result("records").Count & " matching results, using first"
        If Result("records").Count = 0 Then
            Exit Sub
        Else
            row = 5
            For Each Prg In Result("records")
             Cells(row, 6).Value = Prg("wbs_element_id")
             Cells(row, 7).Value = Prg("wbs_element_description")
             row = row + 1
             Next Prg
        End If
    End If
    
    Dim Project As Dictionary
    Set Project = Result("records")(1)

    [Project_code].Value = Project("wbs_element_id")
    [Project_title].Value = Project("wbs_element_description")
    [Project_leader].Value = Project("project_leader_description")
    [Lead_researcher].Value = Project("PC Approver")
    [Funding_source].Value = Project("funding_source_description")
    If Project("wbs_start_date") <> "#" Then
        [Project_Start_Date].Value = Replace(Project("wbs_start_date"), ".", "/")
    End If
    If Project("wbs_end_date") <> "#" Then
        [Project_End_Date].Value = Replace(Project("wbs_end_date"), ".", "/")
    End If
    If (Project("industry_description") <> "Not assigned") Then
        [Keywords].Value = Project("industry_description")
    End If
End Sub

Public Sub MergeFromWorkbook()
    'Find and open the target workbook - then copy over all the sheets
    Dim targetBook As Workbook
    Dim sourceBook As Workbook
    Dim targetName As String
    
    Set targetBook = ActiveWorkbook
    Path = FileBrowse()
    If Path = "" Then
        Exit Sub
    End If
    ' Open method requires full file path to be referenced, but workbook calls only need the name
    targetName = Right(Path, Len(Path) - InStrRev(Path, Application.PathSeparator, , 1))
    Set sourceBook = Workbooks.Open(Path, ReadOnly:=True)
    sourceBook.Sheets.Copy After:=targetBook.Sheets(targetBook.Sheets.Count)
    sourceBook.Close SaveChanges:=False
    Sheet1.Activate
    'UpdateSheetIndex  - don't call this as we don't know for sure we are on ProjectDescription page
End Sub

'-------------------------------------
' Update Data Hub from metadata

Public Sub UpdateDataHub()
    ' Gather metadata from ProjectDescription sheet and upload to PFR DataHub
    Dim Project As Dictionary
    Result = MsgBox("This will gather the metadata from the ProjectDescription Sheet and create or update a corresponding DataHub Record. If Attach Resources is checked then the Workbook will be uploaded otherwise it will be link referenced. ", _
        vbOKCancel + vbInformation, "Register in DataHub")
        
    If Result = vbOK Then
        'Check file is saved or Save the file
        'Does the DataSet already Exist?
        Name = Slug([Title].Text)
        Set ds = GetDataset(Name)
        newPackage = True
        If (ds.Count <> 0) Then
            newPackage = False
            [ErrorMessage].Value = Name & " has id:" & ds("id")
            'Debug.Print (Name & " has id:" & ds("id"))
        End If
        
        
        ' Gather the Metadata
        Set Project = GatherProjectInfo()
        If Project Is Nothing Then
            Exit Sub
        End If
        JsonProject = ConvertToJson(Project, 2)
        Continue = MsgBox(JsonProject, vbOKCancel + vbInformation, "This is what we will send to DataHub")
        
        If Continue = vbOK Then
            ' Send request and get id back
            Set Result = PackageCreate(Project, newPackage)
            If (Result.Count <> 0) Then
                MsgBox "Success: Dataset record created"
                ' update link field with the new id.
                DatahubLink = BaseUrl() & "/dataset/" & Name
                ActiveSheet.Hyperlinks.Add Range("DataHub_Reference"), DatahubLink, "", "", DatahubLink
            End If
        End If
     
    End If
    
End Sub
Function Slug(ByVal str As String) As String
    Slug = Replace(Replace(Replace(Replace(LCase(Trim(str)), """", ""), ".", ""), "'", ""), " ", "-")
End Function
Public Function GatherProjectInfo() As Dictionary
    'Set Coversheet = ThisWorkbook.Worksheets("ProjectDescription")
    'With Coversheet
    Dim Project As New Dictionary
    ActiveWorkbook.Save
    
    ' Title
    If ([Title].Text = "") Then
        MsgBox "Please fill in the Title ", vbOKOnly, "Warning: Incomplete Metadata"
        [Title].Select
        Exit Function
    End If
    Project.Item("title") = [Title].Text
    Project.Item("name") = Slug([Title].Text)
    Project.Item("project_code") = [Project_code].Text
    Project.Item("project_title") = [Project_title].Text
    
     'Tags
    KeywordsTxt = [Keywords].Text
    Dim Words() As String
    Words = Split(KeywordsTxt, ",")
    If UBound(Words, 1) > -1 Then
        Dim Tags() As Dictionary
        ReDim Tags(UBound(Words, 1))
        For i = 0 To UBound(Tags, 1)
            Set Tags(i) = New Dictionary
            Tags(i).Item("name") = Slug(Words(i))
        Next
        Project.Item("tags") = Tags
    Else
        Continue = MsgBox("Adding some keywords will make your dataset easier to find, Continue?", vbOKCancel, "Warning: No Keywords")
        If Continue = vbCancel Then
            Exit Function
        End If
    End If
    
    ' People
    Project.Item("author") = [Project_leader].Text
    Project.Item("lead_researcher") = [Lead_researcher].Text
    Project.Item("maintainer") = [Data_custodian].Text
    Project.Item("data_specialist") = [Data_specialist].Text
    
    
    ' Further Information
    Project.Item("other_codes") = [Other_project_codes].Text
    Project.Item("funding_source") = [Funding_source].Text
    StartDate = [Project_Start_Date].Value
    If StartDate <> "" And IsDate(StartDate) Then
        Project.Item("project_start_date") = FormatISODate(DateValue(StartDate))
    Else
        Continue = MsgBox("Start Date is empty or not a valid date. Continue?", vbOKCancel, "Warning: StartDate")
        If Continue = vbCancel Then
            Exit Function
        End If
    End If
    EndDate = [Project_End_Date].Value
    If EndDate <> "" And IsDate(EndDate) Then
        Project.Item("project_end_date") = FormatISODate(DateValue(EndDate))
    Else
        Continue = MsgBox("End Date is empty or not a valid date. Continue?", vbOKCancel, "Warning: EndDate")
        If Continue = vbCancel Then
            Exit Function
        End If
    End If
    Project.Item("other_codes") = [Tracking_codes].Text
    Project.Item("other_researchers") = [Other_researchers].Text
    Project.Item("location") = [Location].Text
    
    'Descripton
    Text = [Description_table].Value
    Description = ""
    For Each line In Text
        Description = Description & vbLf & blLf & line
    Next
    Description = Description
    Project.Item("notes") = Description
        
    'DataHub Settings
    Project.Item("private") = "true" ' private by default
    If [Publish].Text = "Public" Then
        Project.Item("private") = "false"
    End If
    Project.Item("owner_org") = "pfr"
    
    'Resource Values
    'This Workbook  (we will gather the rest later)
    Dim Resources() As Variant
    ReDim Preserve Resources(0)
    Set Resources(0) = GatherResourceInfo(1, [PrimaryResource])
    resCtr = 1
    
    ' Raw Data Resources
    rawData = [AssociatedRawData]
        For i = 1 To UBound(rawData)
            If (rawData(i, 2) <> "") Then
                ReDim Preserve Resources(0 To resCtr)

                Set Resources(resCtr) = GatherResourceInfo(i, rawData)
                resCtr = resCtr + 1
            End If
        Next i

    ' Other Data Resources
    otherResources = [otherResources]
        For i = 1 To UBound(otherResources)
            If (otherResources(i, 2) <> "") Then
                ReDim Preserve Resources(0 To resCtr)
                Set Resources(resCtr) = GatherResourceInfo(i, otherResources)
                resCtr = resCtr + 1
            End If
        Next i
    Project.Item("resources") = Resources
    Set GatherProjectInfo = Project
    'End With
End Function
Public Function GatherResourceInfo(row, rng)
    Dim Resource As Dictionary
    Set Resource = New Dictionary
    Resource.Item("format") = rng(row, 1)
    Resource.Item("name") = rng(row, 2)
    Resource.Item("url") = rng(row, 3)
    Resource.Item("description") = rng(row, 4)
    'Resource.Item("mimetype") = "application/vnd.ms-excel"
    Resource.Item("url_type") = "link"
    Set GatherResourceInfo = Resource
End Function

Public Function GatherRawResources()
    rawData = [AssociatedRawData]
    For i = 1 To UBound(rawData)
        Set res = GatherResourceInfo(i, rawData)
    Next i

    'For E    GatherPrimaryResource = GatherResourceInfo([PrimaryResource])
End Function
Public Function GetDataset(Name)
    Dim CkanClient As New WebClient
    CkanClient.BaseUrl = BaseUrl() & "/api/3/action/"
    [ErrorMessage].Value = ""
    
    Dim Request As New WebRequest
    Request.Method = WebMethod.HttpGet
    Request.Resource = "package_show?id=" & Name
    Request.Format = WebFormat.Json
    Request.AddHeader "Authorization", CkanApiKey()
    
    ' Enable logging before the request is executed
    WebHelpers.EnableLogging = True
    
    Dim Result As Dictionary
    
    ' Use GetJSON helper to execute simple request and work with response
    Dim Response As WebResponse
    Set Response = CkanClient.Execute(Request)
    Debug.Print Response.StatusCode
    If Response.StatusCode <> WebStatusCode.Ok Then
        [ErrorMessage].Value = "Error:" & Response.StatusCode
        Set GetDataset = New Dictionary
        Exit Function
    End If
    Set Result = Response.Data("result")
    Set GetDataset = Result
End Function
Function FormatISODate(ByRef dateTime As Date)
    FormatISODate = Format(dateTime, "yyyy-mm-ddThh:mm:ss")
End Function

Function PackageCreate(ByRef dataset As Dictionary, ByVal newPackage As Boolean)
'
' PackageCreate Macro
' Call API to upload a new dataset
'
    Debug.Print "PackageCreate"; newPackage
    Dim CkanClient As New WebClient
    CkanClient.BaseUrl = BaseUrl()
    [ErrorMessage].Value = ""

    Dim Request As New WebRequest
    If (newPackage) Then
        Request.Resource = "/api/3/action/package_create"
    Else
        Request.Resource = "/api/3/action/package_update"
    End If
    
    Request.Method = WebMethod.HttpPost
    Request.Format = WebFormat.Json
    Request.AddHeader "Authorization", CkanApiKey()
    Set Request.Body = dataset
    
    ' Enable logging before the request is executed
    WebHelpers.EnableLogging = True
    
    ' Use GetJSON helper to execute simple request and work with response
    Dim Response As WebResponse
    Set Response = CkanClient.Execute(Request)
    If Response.StatusCode <> WebStatusCode.Ok Then
        MsgBox "Problem uploading to DataHub: " & Response.StatusCode
        [ErrorMessage].Value = Response.StatusCode
        Set PackageCreate = New Dictionary
        Exit Function
    End If

    Set PackageCreate = Response.Data("result")

End Function
Function xPackageCreate(ByRef dataset As Dictionary)
'
' PackageCreate Macro
' Call API to upload a new dataset
'
    Dim CkanClient As New WebClient
    ' Enable logging before the request is executed
    WebHelpers.EnableLogging = True
    
    'CkanClient.BaseUrl = BaseUrl()
    [ErrorMessage].Value = ""
    
    Dim Headers As New Collection
    Headers.Add WebHelpers.CreateKeyValue("Authorization", CkanApiKey())
    
    Dim Options As New Dictionary
    Options.Add "Headers", Headers
    
    ' Use GetJSON helper to execute simple request and work with response
    Dim Response As WebResponse
    Set Response = CkanClient.PostJson(BaseUrl() & "/api/3/action/package_create", dataset, Options)
    If Response.StatusCode <> WebStatusCode.Ok Then
        [ErrorMessage].Value = Response.StatusCode
        Set PackageCreate = New Dictionary
        Exit Function
    End If

    Set PackageCreate = Response.Data("result")

End Function



