Attribute VB_Name = "PFRDataHub"
' Plant and Food Research - DataHub Macros
' This module provides supporting macros for the Metadata Coversheet template
' This template provides a cover sheet for any Excel workbook that is used to hold research data
' The cover sheet captures key metadata about the project and allows the information to be registered into the PFR DataHub.

Dim CollectionsList() As String
Dim GroupsList() As String

Public Sub ReportErr(StatusCode, StatusDescription)
    MsgBox "Problem connecting to DataHub: " & StatusCode & " " & StatusDescription
    [ErrorMessage].Value = StatusDescription
End Sub


Public Sub GetProjectCode()
    ClearSearch
    search = [Project_code].Value
    test = [TestMode].Value
    Call GetProjectCodeRefAPI(search, test)
End Sub
Public Sub FindPerson(name As String)
    ClearSearch
    MsgBox "Find Person " & name
    test = [TestMode].Value
    ' Call GetProjectCodeRefAPI(search, test)
End Sub

Public Sub ClearMetadata()
    ' wipe out the filled in fields so that the sheet is clean as the original template
    [MainMetadata].Value = ""
    [SearchResults].Value = ""
    [AssociatedRawData].Value = ""
    [OtherResources].Value = ""
    [DataHub_ID].Value = ""
    [DataHub_Reference].Value = ""
    [FullName].Value = Application.Username
    [DataHub_Link].Value = "http://datahub.plantandfood.co.nz"
    GetCollections
    GetGroups
End Sub
Public Sub ClearSearch()
    ' wipe out the search results
    [SearchResults].Value = ""
    [Name_results].Value = ""
End Sub

'-------------------------------------
' Update Data Hub from metadata

Public Sub UpdateDataHub()
    ' Gather metadata from ProjectDescription sheet and upload to PFR DataHub
    Dim Project As Dictionary
    Result = MsgBox("This will gather the metadata from the ProjectDescription Sheet and create or update a corresponding DataHub Record. If Attach Resources is checked then the Workbook will be uploaded otherwise it will be link referenced. ", _
        vbOKCancel + vbInformation, "Register in DataHub")
        
    If Result = vbOK Then
        'Check file is saved or Save the file
        'Does the DataSet already Exist?
        name = Slug([Title].Text) & "_" & Slug([Project_code].Text)
        DataHubID = [DataHub_ID].Text
        
        newPackage = True
        If (DataHubID <> "") Then
            Set ds = GetDataset(DataHubID)
             If (ds.Count <> 0) Then
                newPackage = False
                [ErrorMessage].Value = "Found: " & name & " has id:" & ds("id")
            End If
        Else
            ' if we find the name we may have a duplicate
            Set ds = GetDataset(name)
            If (ds.Count <> 0) Then
                newPackage = False
                [ErrorMessage].Value = "Found: " & name & " has id:" & ds("id")
                MsgBox "A dataset titled " & name & " already exists.  Please provide an alternative", vbOKOnly, "Duplicate Title"
                Exit Sub
            End If
        End If
        
        ' Gather the Metadata
        Set Project = GatherProjectInfo(DataHubID)
        If Project Is Nothing Then
            Exit Sub
        End If
        JsonProject = ConvertToJson(Project, 2)
        Continue = MsgBox(JsonProject, vbOKCancel + vbInformation, "This is what we will send to DataHub")
        
        If Continue = vbOK Then
            ' Send request and get id back
            Set Result = PackageCreate(Project, newPackage)
            If (Result.Count <> 0) Then
                If newPackage Then
                    MsgBox "Success: Dataset record " & name & " created", vbOKOnly, "PFR DataHub"
                Else
                    MsgBox "Success: Dataset record " & name & " updated", vbOKOnly, "PFR DataHub"
                End If
                ' update link field with the new id.
                DatahubLink = DataHubHost() & "/dataset/" & name
                ActiveSheet.Hyperlinks.Add Range("DataHub_Reference"), DatahubLink, "", "", DatahubLink
                [DataHub_ID].Value = Result("id")
            End If
        End If
     
    End If
    
End Sub
Function Slug(ByVal str As String) As String
    Slug = Replace(Replace(Replace(Replace(Replace(LCase(Trim(str)), """", ""), ".", ""), "'", ""), " ", "-"), "/", "-")
End Function
Public Function GatherProjectInfo(id) As Dictionary
    'Set Coversheet = ThisWorkbook.Worksheets("ProjectDescription")
    'With Coversheet
    Dim Project As New Dictionary
    ActiveWorkbook.Save
    GetLists
    
    ' Title
    If ([Title].Text = "" Or [Project_code].Text = "") Then
        MsgBox "Please fill in the Title and project code", vbOKOnly, "Warning: Incomplete Metadata"
        [Title].Select
        Exit Function
    End If
    If (id <> "") Then
        Project.Item("id") = id
    End If
    
    Project.Item("title") = [Title].Text
    Project.Item("name") = Slug([Title].Text) & "_" & Slug([Project_code].Text)
    Project.Item("project_code") = [Project_code].Text
    Project.Item("project_title") = [project_title].Text
    
     'Tags
    KeywordsTxt = [Keywords].Text
    Dim Words() As String
    Words = Split(KeywordsTxt, ",")
    If UBound(Words, 1) > -1 Then
        Dim Tags() As Dictionary
        ReDim Tags(UBound(Words, 1))
        For i = 0 To UBound(Tags, 1)
            Set Tags(i) = New Dictionary
            Tags(i).Item("name") = Slug(Words(i))
        Next
        Project.Item("tags") = Tags
    Else
        Continue = MsgBox("Adding some keywords will make your dataset easier to find, Continue?", vbOKCancel, "Warning: No Keywords")
        If Continue = vbCancel Then
            Exit Function
        End If
    End If
    
    ' Collection
    ' read the collection names from the worksheet
    With ActiveSheet.DropDowns("CollectionComboBox")
        Project.Item("owner_org") = CollectionsList(.Value)
    End With
    
    ' Groups
    With ActiveSheet.ListBoxes("GroupsListBox")
        If (.ListCount <> 0) Then
            Dim Groups() As Dictionary
            
            Dim grp As Long
            SelectedIndex = 0
            For grp = 1 To .ListCount
                  If .Selected(grp) = True Then
                    ReDim Preserve Groups(SelectedIndex)
                    Set Groups(SelectedIndex) = New Dictionary
                    Groups(SelectedIndex).Item("name") = GroupsList(grp)
                    SelectedIndex = SelectedIndex + 1
                End If
            Next grp
            Project.Item("groups") = Groups
        End If
    End With
    
    ' People
    Project.Item("author") = [Project_leader].Text
    Project.Item("lead_researcher") = [Lead_researcher].Text
    Project.Item("maintainer") = [Data_custodian].Text
    Project.Item("data_specialist") = [Data_specialist].Text
    Project.Item("business_manager") = [Business_manager].Text
    
    ' Further Information
    Project.Item("other_codes") = [Other_project_codes].Text
    Project.Item("funding_source") = [Funding_source].Text
    StartDate = [project_start_date].Value
    If StartDate <> "" And IsDate(StartDate) Then
        Project.Item("project_start_date") = FormatISODate(DateValue(StartDate))
    Else
        Continue = MsgBox("Start Date is empty or not a valid date. Continue?", vbOKCancel, "Warning: StartDate")
        If Continue = vbCancel Then
            Exit Function
        End If
    End If
    EndDate = [project_end_date].Value
    If EndDate <> "" And IsDate(EndDate) Then
        Project.Item("project_end_date") = FormatISODate(DateValue(EndDate))
    Else
        Continue = MsgBox("End Date is empty or not a valid date. Continue?", vbOKCancel, "Warning: EndDate")
        If Continue = vbCancel Then
            Exit Function
        End If
    End If
    Project.Item("other_codes") = [Tracking_codes].Text
    Project.Item("other_researchers") = [Other_researchers].Text
    Project.Item("location") = [Location].Text
    
    'Descripton
    Text = [Description_table].Value
    Description = ""
    For Each line In Text
        Description = Description & vbLf & blLf & line
    Next
    Description = Description
    Project.Item("notes") = Description
        
    'DataHub Settings
    Project.Item("private") = "true" ' private by default
    If [Publish].Text = "Public" Then
        Project.Item("private") = "false"
    End If
    
    'Resource Values
    'This Workbook  (we will gather the rest later)
    Dim Resources() As Variant
    ReDim Preserve Resources(0)
    Set Resources(0) = GatherResourceInfo(1, [PrimaryResource])
    resCtr = 1
    
    ' Raw Data Resources
    rawData = [AssociatedRawData]
    For i = 1 To UBound(rawData)
        If (rawData(i, 2) <> "") Then
            ReDim Preserve Resources(0 To resCtr)
            Set Resources(resCtr) = GatherResourceInfo(i, rawData)
            resCtr = resCtr + 1
        End If
    Next i

    ' Other Data Resources
    otherRes = [OtherResources]
    For i = 1 To UBound(otherRes)
        If (otherRes(i, 2) <> "") Then
            ReDim Preserve Resources(0 To resCtr)
            Set Resources(resCtr) = GatherResourceInfo(i, otherRes)
            resCtr = resCtr + 1
        End If
    Next i
    Project.Item("resources") = Resources
    Set GatherProjectInfo = Project
End Function
Public Function GatherResourceInfo(row, rng)
    Dim Resource As Dictionary
    Set Resource = New Dictionary
    Resource.Item("format") = rng(row, 1)
    Resource.Item("name") = rng(row, 2)
    Resource.Item("url") = rng(row, 3)
    Resource.Item("description") = rng(row, 4)
    'Resource.Item("mimetype") = "application/vnd.ms-excel"
    Resource.Item("url_type") = "link"
    Set GatherResourceInfo = Resource
End Function

Public Function GatherRawResources()
    rawData = [AssociatedRawData]
    For i = 1 To UBound(rawData)
        Set res = GatherResourceInfo(i, rawData)
    Next i
    'For E    GatherPrimaryResource = GatherResourceInfo([PrimaryResource])
End Function
Public Function GetDataset(name)
    Dim CkanClient As New WebClient
    CkanClient.BaseUrl = DataHubHost() & "/api/3/action/"
    [ErrorMessage].Value = ""
    
    Dim Request As New WebRequest
    Request.Method = WebMethod.HttpGet
    Request.Resource = "package_show?id=" & name
    Request.Format = WebFormat.Json
    Request.AddHeader "Authorization", CkanApiKey()
    
    ' Enable logging before the request is executed
    WebHelpers.EnableLogging = True
    
    Dim Result As Dictionary
    
    ' Use GetJSON helper to execute simple request and work with response
    Dim Response As WebResponse
    Set Response = CkanClient.Execute(Request)
    Debug.Print Response.StatusCode
    If Response.StatusCode <> WebStatusCode.Ok Then
        Set ErrorResult = Response.Data("error")
        [ErrorMessage].Value = Response.StatusCode
        Set GetDataset = New Dictionary
        Exit Function
    End If
    Set Result = Response.Data("result")
    Set GetDataset = Result
End Function
Function FormatISODate(ByRef dateTime As Date)
    FormatISODate = Format(dateTime, "yyyy-mm-ddThh:mm:ss")
End Function

Function PackageCreate(ByRef dataset As Dictionary, ByVal newPackage As Boolean)
'
' PackageCreate Macro
' Call API to upload a new dataset
'
    Debug.Print "PackageCreate"; newPackage
    Dim CkanClient As New WebClient
    CkanClient.BaseUrl = DataHubHost()
    [ErrorMessage].Value = ""

    Dim Request As New WebRequest
    If (newPackage) Then
        Request.Resource = "/api/3/action/package_create"
    Else
        Request.Resource = "/api/3/action/package_update"
    End If
    
    Request.Method = WebMethod.HttpPost
    Request.Format = WebFormat.Json
    Request.AddHeader "Authorization", CkanApiKey()
    Set Request.Body = dataset
    
    ' Enable logging before the request is executed
    WebHelpers.EnableLogging = True
    
    ' Use GetJSON helper to execute simple request and work with response
    Dim Response As WebResponse
    Set Response = CkanClient.Execute(Request)
    If Response.StatusCode <> WebStatusCode.Ok Then
        MsgBox "Problem uploading to DataHub: " & Response.StatusCode
        [ErrorMessage].Value = Response.StatusCode
        Set PackageCreate = New Dictionary
        Exit Function
    End If

    Set PackageCreate = Response.Data("result")

End Function
