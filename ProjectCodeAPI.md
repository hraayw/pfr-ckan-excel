# Project code API

## Get project by Project code
curl -X GET "http://iris.uat.pfr.co.nz:8800/api/projects/?local_project_identifier=P%2F311042%2F01" -H "accept: application/json" -H "X-CSRFToken: j2ikzGZcUVkvZ2dDq7MYLBGYbqDG6OJje2TU6eWTcs1mdZ8ryoN8XflD6mtF5UJW"

### Response
{
  "count": 1,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 655,
      "project_title": "CLSD HEINZ WATTIES- INFECTED BEETROOT",
      "project_description": "Funded by Commercial Other for the Vegetables* industry with Heinz Wattie's Limited as the customer.",
      "keywords": null,
      "project_start_date": "2019-07-01T00:00:00Z",
      "project_end_date": "2019-07-31T00:00:00Z",
      "local_project_identifier": "P/311042/01",
      "parent_local_project": null,
      "project_status": {
        "id": 7,
        "code": "107",
        "description": "Closed",
        "definition": "Created automatically because the supplied NZRIS statuses make no sense",
        "guide_for_use": null
      }
    }
  ]
}

## get staff on project
curl -X GET "http://iris.uat.pfr.co.nz:8800/api/projects/655/assignedstaff/" -H "accept: application/json" -H "X-CSRFToken: j2ikzGZcUVkvZ2dDq7MYLBGYbqDG6OJje2TU6eWTcs1mdZ8ryoN8XflD6mtF5UJW"

### response
{
  "count": 4,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 26147,
      "person": {
        "id": 72,
        "given_name": "HAYLEY",
        "other_given_names": null,
        "family_name": "RIDGWAY",
        "date_of_birth": null,
        "gender": null,
        "localID": "2075",
        "gender_other_information": null,
        "prior_local_person": null,
        "ethnicity": null,
        "iwi_affiliation": null
      },
      "role": {
        "id": 7,
        "code": "R1",
        "description": "Lead Contributor",
        "definition": "The contributor with overall responsibility for delivering the project. For R&D project, this refers to the Principal Investigator or Lead Scientist",
        "guide_for_use": null
      },
      "start_date": null,
      "end_date": null,
      "FTE_allocation": null,
      "local_role_description": null
    },
    {
      "id": 26146,
      "person": {
        "id": 78,
        "given_name": "FARHAT",
        "other_given_names": null,
        "family_name": "SHAH",
        "date_of_birth": null,
        "gender": null,
        "localID": "1476",
        "gender_other_information": null,
        "prior_local_person": null,
        "ethnicity": null,
        "iwi_affiliation": null
      },
      "role": {
        "id": 10,
        "code": "S0",
        "description": "Supporting Staff",
        "definition": "Supporting staff includes skilled and unskilled craftsmen, and administrative, secretarial and clerical staff participating in or directly associated with the project",
        "guide_for_use": null
      },
      "start_date": "2019-07-01T00:00:00Z",
      "end_date": "2019-07-31T00:00:00Z",
      "FTE_allocation": "0.37",
      "local_role_description": null
    },
    {
      "id": 26148,
      "person": {
        "id": 72,
        "given_name": "HAYLEY",
        "other_given_names": null,
        "family_name": "RIDGWAY",
        "date_of_birth": null,
        "gender": null,
        "localID": "2075",
        "gender_other_information": null,
        "prior_local_person": null,
        "ethnicity": null,
        "iwi_affiliation": null
      },
      "role": {
        "id": 10,
        "code": "S0",
        "description": "Supporting Staff",
        "definition": "Supporting staff includes skilled and unskilled craftsmen, and administrative, secretarial and clerical staff participating in or directly associated with the project",
        "guide_for_use": null
      },
      "start_date": "2019-07-01T00:00:00Z",
      "end_date": "2019-07-31T00:00:00Z",
      "FTE_allocation": "0.18",
      "local_role_description": null
    },
    {
      "id": 37218,
      "person": {
        "id": 5231,
        "given_name": "HAYLEY",
        "other_given_names": null,
        "family_name": "RIDGWAY",
        "date_of_birth": null,
        "gender": null,
        "localID": "P/311042",
        "gender_other_information": null,
        "prior_local_person": null,
        "ethnicity": null,
        "iwi_affiliation": null
      },
      "role": {
        "id": 10,
        "code": "S0",
        "description": "Supporting Staff",
        "definition": "Supporting staff includes skilled and unskilled craftsmen, and administrative, secretarial and clerical staff participating in or directly associated with the project",
        "guide_for_use": null
      },
      "start_date": "2019-07-01T00:00:00Z",
      "end_date": "2019-07-31T00:00:00Z",
      "FTE_allocation": "0.36",
      "local_role_description": null
    }
  ]
}